

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConditionDemo1 {
	public static void main(String[] args) throws InterruptedException {
		Car car = new Car();
		ExecutorService exec = Executors.newCachedThreadPool();
		exec.execute(new WaxOnRunnable(car));
		exec.execute(new WaxOffRunnable(car));

		Thread.sleep(100);
		exec.shutdownNow();
	}
}

class WaxOnRunnable implements Runnable {

	private Car car;

	public WaxOnRunnable(Car car) {
		super();
		this.car = car;
	}

	public void run() {
		car.lock.lock();
		try {
			while (!Thread.interrupted()) {
				car.waitForWaxing();
				car.waxed();
//				TimeUnit.MICROSECONDS.sleep(100);
			}
		} catch (InterruptedException e) {
			System.out.println("waxon thread interrupted");
			e.printStackTrace();
		} finally {
			car.lock.unlock();
		}

	}

}

class WaxOffRunnable implements Runnable {

	private Car car;

	public WaxOffRunnable(Car car) {
		super();
		this.car = car;
	}

	public void run() {
		car.lock.lock();
		try {
			while (!Thread.interrupted()) {
				car.waitForBuffing();
				car.buffed();
//				TimeUnit.MICROSECONDS.sleep(100);
			}
		} catch (InterruptedException e) {
			System.out.println("waxoff thread interrupted");
			e.printStackTrace();
		} finally {
			car.lock.unlock();
		}

	}

}

class Car {
	private boolean waxOn = false;

	Lock lock = new ReentrantLock();
	Condition condition = lock.newCondition();

	public void waxed() {
		lock.lock();

		System.out.println("waxing");
		waxOn = true;
		try {
			condition.signalAll();
		} finally {
			lock.unlock();
		}

	}

	public void buffed() {
		lock.lock();
		System.out.println("wax off");
		waxOn = false;
		try {
			condition.signalAll();
		}finally {
			lock.unlock();
		}
	}

	public void waitForWaxing() throws InterruptedException {
		lock.lock();
		try {
			while (waxOn) {
				condition.await();
			}
		}
		finally {
			lock.unlock();
		}
		
	}

	public void waitForBuffing() throws InterruptedException {
		lock.lock();
		try {
			while (!waxOn) {
				condition.await();
			}
		}
		finally {
			lock.unlock();
		}
		
	}

}