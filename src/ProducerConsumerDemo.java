import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ProducerConsumerDemo {
    public static void main(String[] args) {
        new Restaurant();
    }
}

class Meal {
    private int orderNum = 0;

    public Meal(int orderNum) {
        this.orderNum = orderNum;
    }

    public String toString() {
        return "Meal: " + orderNum;
    }
}

class Chef implements Runnable {

    int count;
    Restaurant restaurant;

    public Chef(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public void run() {
        try {
            // without this condition the program will run only once
            while (!Thread.interrupted()) {
                synchronized (this) {
                    while (restaurant.getMeal() != null) {
                        wait();
                    }
                }
                if (++count == 10) {
                    System.out.println("created maxinmum meals for the day. Closing...");
                    restaurant.executor.shutdownNow();
                }
                System.out.print("Order up");
                // notifying the chef to produce next meal
                synchronized (restaurant.getWaitPerson()) {
                    restaurant.setMeal(new Meal(count));
                    restaurant.getWaitPerson().notifyAll();
                }
            }
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

class WaitPerson implements Runnable {

    Restaurant restaurant;

    public WaitPerson(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                synchronized (this) {
                    while (restaurant.getMeal() == null) {
                        wait();
                    }
                }
                System.out.println("waitperson got " + restaurant.getMeal());
                // notifying the chef to produce next meal
                synchronized (restaurant.getChef()) {
                    restaurant.setMeal(null);
                    restaurant.getChef().notifyAll();
                }
            }
        } catch (InterruptedException e) {
            System.out.println("waitperson interrupted");
        }

    }
}

class Restaurant {
    ExecutorService executor = Executors.newCachedThreadPool();
    private Meal meal;
    private Chef chef = new Chef(this);
    private WaitPerson waitPerson = new WaitPerson(this);

    public Restaurant() {
        executor.execute(chef);
        executor.execute(waitPerson);
    }

    public ExecutorService getExecutor() {
        return executor;
    }

    public Meal getMeal() {
        return meal;
    }

    public Chef getChef() {
        return chef;
    }

    public WaitPerson getWaitPerson() {
        return waitPerson;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }
}
