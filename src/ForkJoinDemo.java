import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

class MyRecursiveAction extends RecursiveAction {
    private final static int THRESHOLD = 5;
    private int startIndex;
    private int lastIndex;
    private int[] data;

    public MyRecursiveAction(int startIndex, int lastIndex, int[] data) {
	super();
	this.startIndex = startIndex;
	this.lastIndex = lastIndex;
	this.data = data;
    }

    @Override
    protected void compute() {
	if (lastIndex - startIndex <= THRESHOLD) {
	    for (int i = startIndex; i <= lastIndex; i++) {
		System.out.println(Thread.currentThread().getName() + " " + i);
	    }
	} else {
	    int mid = (startIndex + lastIndex) / 2;
	    MyRecursiveAction a1 = new MyRecursiveAction(startIndex, mid, data);
	    a1.fork();
	    MyRecursiveAction a2 = new MyRecursiveAction(mid + 1, lastIndex, data);
	    a2.compute();
	    a1.join();
	}
    }

}

public class ForkJoinDemo {
    public static void main(String[] args) {
	int[] data = new int[20];
	MyRecursiveAction action = new MyRecursiveAction(0, data.length - 1, data);
	ForkJoinPool forkJoinPool = new ForkJoinPool();
	forkJoinPool.invoke(action);
    }
}
