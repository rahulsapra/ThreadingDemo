import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;

public class BlockingQueueDemo {

	public static void test(int id, String message, BlockingQueue<Vehicle> blockingQueue) {
		System.out.println("message: " + message);
		VehicleQueueRunnable queue = new VehicleQueueRunnable(blockingQueue);
		Thread t = new Thread(queue);
		t.start();
		for (int i = 0; i < 2; i++) {
			queue.add(new Vehicle(id, message));
		}
		// t.interrupt();
		System.out.println("test method ends");
	}

	public static void main(String[] args) {
		test(1, "a", new ArrayBlockingQueue<>(10));
		// test(2,"b", new LinkedBlockingQueue());
		// test(3,"c", new SynchronousQueue<>());
	}
}

class VehicleQueueRunnable implements Runnable {
	private BlockingQueue vehicleQueue;

	public VehicleQueueRunnable() {
	}

	public VehicleQueueRunnable(BlockingQueue vehicleQueue) {
		this.vehicleQueue = vehicleQueue;
	}

	public void add(Vehicle v) {
		try {
			vehicleQueue.put(v);
			System.out.println("vehicleQueue: " + vehicleQueue.getClass().getName());
			System.out.println(vehicleQueue.size());
		} catch (InterruptedException e) {
			// TODO: handle exception
			System.out.println("add interrupted");
		}
	}

	public void remove(Vehicle v) {
		try {
			Vehicle vehicle = (Vehicle) vehicleQueue.take();
			System.out.println("vehicle: " + vehicle.getName());
		} catch (InterruptedException e) {
			System.out.println("remove interrupted");
		}

	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				// if list is empty. it will wait till some element is added.
				vehicleQueue.take();
			}
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}

	}
}

class Vehicle {
	private int id;
	private String name;

	public Vehicle(int id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}