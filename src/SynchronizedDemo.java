
public class SynchronizedDemo {
	int i;

	synchronized public void increment() {
		i++;
		System.out.println(i);
	}

	public void decrement() {
		synchronized (this) {
			i--;
		}
		System.out.println(i);
	}

	public static void main(String[] args) {
		SynchronizedDemo sd = new SynchronizedDemo();
		sd.increment();
		sd.decrement();
	}
}
