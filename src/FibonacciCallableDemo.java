import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FibonacciCallableDemo {
	public static void main(String[] args) {
		ExecutorService executor = Executors.newFixedThreadPool(3);
		List<Future<String>> results = new ArrayList<>();
		for (int i = 1; i < 10; i++) {
			results.add(executor.submit(new FibonacciCallable(i)));
		}
		for (Future<String> str : results) {
			// checking if the task has completed
			if (str.isDone()) {
				try {
					System.out.println(str.get());
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					executor.shutdown();
				}
			}
		}
	}
}

class FibonacciCallable implements Callable<String> {
	int n;

	public FibonacciCallable() {
	}

	public FibonacciCallable(int n) {
		this.n = n;
	}

	public String call() {
		return "\nN: " + n + " " + fibonacci(n);
	}

	public String fibonacci(int n) {
		String str = "";
		int first = 0;
		int second = 1;
		str += first + " " + second + " ";
		for (int i = 2; i < n; i++) {
			int next = first + second;
			str += next + " ";
			first = second;
			second = next;
		}
		return str;
	}
}
