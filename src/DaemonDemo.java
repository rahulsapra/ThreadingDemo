
public class DaemonDemo {
	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			Thread daemon = new Thread(new DaemonRunnable());
			daemon.setDaemon(true);
			daemon.start();
		}
		System.out.println("all daemons started");
	}
}

class DaemonRunnable implements Runnable {

	@Override
	public void run() {
		System.out.println("hello");

	}

}
