import java.util.concurrent.atomic.AtomicInteger;

public class AtomicClassesDemo {
	public static void main(String[] args) {
		AtomicInteger i = new AtomicInteger(10);
//		System.out.println(i.getAndAdd(3)); //returns the previous value of i
		System.out.println(i.addAndGet(3)); //returns the updated value of i after the addition
		System.out.println(i.get());
		i.getAndIncrement();
		System.out.println(i.get());
	}
}
