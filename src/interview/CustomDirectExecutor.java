package interview;

import java.util.concurrent.Executor;

/**
 * implements the runnable directly no logic applied
 */
public class CustomDirectExecutor implements Executor {


    @Override
    public void execute(Runnable command) {
        command.run();
    }
}
