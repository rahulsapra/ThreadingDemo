package interview;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class CustomExecutor {

    /**
     * workerqueue to store tasks added to the executor
     */
    private BlockingQueue<Runnable> workerQueue = new LinkedBlockingDeque<>();

    /**
     * size of the thread pool
     */
    private int poolSize;

    /**
     * list of available workers executing the tasks
     */
    private List<Worker> workers;

    public CustomExecutor(int poolSize) {
        this.poolSize = poolSize;
        workers = new ArrayList<>(poolSize);
        for (int i = 0; i < poolSize; i++) {
            workers.add(new Worker());
        }
    }

    public void execute(Runnable command) {
        if (command != null) {
            workerQueue.add(command);
        } else {
            throw new NullPointerException("the runnable command cannot be null");
        }
    }

    private class Worker implements Runnable {

        /**
         * the status of the worker, whether it is active or not
         */
        private volatile boolean isActive = true;

        public Worker() {
            new Thread(this::run).start();
        }

        @Override
        public void run() {
            while (isActive()) {
                try {
                    workerQueue.take().run();
                } catch (InterruptedException ex) {
                    return;
                }
            }

        }

        public void setActive(boolean active) {
            isActive = active;
        }

        public boolean isActive() {
            return isActive;
        }
    }

    public void shutDown() {
        for (Worker worker : workers) {
            worker.setActive(false);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        CustomExecutor customExecutor = new CustomExecutor(10);
        for (int i = 0; i < 30; i++) {
            customExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println("thread: " + Thread.currentThread().getThreadGroup() + "-" + Thread.currentThread().getName());
                }
            });
        }
        customExecutor.shutDown();
        System.out.println("trying to push the thread after shutdown");
        customExecutor.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("------------");
            }
        });
    }

}
