package interview;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class CustomCountDownLatch {
	private int count;

	// public CustomCountDownLatch() {
	// // TODO Auto-generated constructor stub
	// }

	public CustomCountDownLatch(int count) {
		super();
		this.count = count;
	}

	public synchronized void await() throws InterruptedException {
		while (count > 0) {
			this.wait();
		}
	}

	public synchronized void countdown() {
		count--;
		if (count == 0) {
			this.notifyAll();
		}
	}

}

class CustomCountDownLatchExecutingTask implements Runnable {

	private CustomCountDownLatch customCountDownLatch;

	public CustomCountDownLatchExecutingTask() {
		// TODO Auto-generated constructor stub
	}

	public CustomCountDownLatchExecutingTask(CustomCountDownLatch customCountDownLatch) {
		super();
		this.customCountDownLatch = customCountDownLatch;
	}

	@Override
	public void run() {
		System.out.println("implementing task");
		customCountDownLatch.countdown();
	}
}

class CustomCountDownLatchWaitingTask implements Runnable {

	private CustomCountDownLatch customCountDownLatch;

	public CustomCountDownLatchWaitingTask() {
		// TODO Auto-generated constructor stub
	}

	public CustomCountDownLatchWaitingTask(CustomCountDownLatch customCountDownLatch) {
		super();
		this.customCountDownLatch = customCountDownLatch;
	}

	@Override
	public void run() {
		try {
			customCountDownLatch.await();
			System.out.println("running the waiting task: " + Thread.currentThread());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}

class MainClass  {
	public static void main(String[] args) {
		CustomCountDownLatch customCountDownLatch = new CustomCountDownLatch(5);
		ExecutorService executor = Executors.newCachedThreadPool();
		for(int i=0; i<5; i++) {
			executor.execute(new CustomCountDownLatchExecutingTask(customCountDownLatch));
			executor.execute(new CustomCountDownLatchWaitingTask(customCountDownLatch));
		}
		executor.shutdown();
		
		
	}
}

