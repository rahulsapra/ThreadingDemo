package interview;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * if two threads have same instance of the iterator and thread 1 removes an
 * element, an exception will be thrown in which the exception would be thrown
 *
 */
public class IteratorQuestion {

	public static void main(String args[]) {
		List<String> myList = new ArrayList<String>();
		for (int i = 0; i < 1000; i++) {
			myList.add("" + i);
		}

		Iterator<String> it = myList.iterator();

		mythread1 myt1 = new mythread1(it);
		mythread2 myt2 = new mythread2(it);
		Thread t1 = new Thread(myt1, "t1");
		Thread t2 = new Thread(myt2, "t2");
		t1.start();
		t2.start();
	}

}

class mythread1 implements Runnable {
	Iterator<String> it;

	public mythread1(Iterator<String> it) {
		this.it = it;
	}

	public void run() {
		while (it.hasNext()) {
			String value = it.next();
			it.remove();
			System.out.println("Thread: " + Thread.currentThread().getName() + "value " + value);

		}

	}
}

class mythread2 implements Runnable {
	Iterator<String> it;

	public mythread2(Iterator<String> it) {
		this.it = it;
	}

	public void run() {
		while (it.hasNext()) {
			String value = it.next();
			System.out.println("Thread: " + Thread.currentThread().getName() + "value " + value);

		}

	}
}
