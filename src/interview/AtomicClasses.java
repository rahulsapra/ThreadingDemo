package interview;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

class EvenOddAtomic {
	private AtomicInteger nextEven = new AtomicInteger(0);
	private AtomicInteger nextOdd = new AtomicInteger(1);

	public void printNext() {
		System.out.println(nextEven.get());
		// nextEven.addAndGet(2);
		nextEven.getAndAdd(2);
		System.out.println(nextOdd.get());
		// nextOdd.addAndGet(2);
		nextOdd.getAndAdd(2);
	}

}

class EvenOddAtomicRunnable implements Runnable {

	EvenOddAtomic evenOddAtomic = new EvenOddAtomic();

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (!Thread.interrupted()) {
			try {
				TimeUnit.MILLISECONDS.sleep(100);
				evenOddAtomic.printNext();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}

public class AtomicClasses {
	public static void main(String[] args) {
		EvenOddAtomicRunnable evenOddAtomicRunnable = new EvenOddAtomicRunnable();
		ExecutorService executor = Executors.newCachedThreadPool();
		executor.execute(evenOddAtomicRunnable);
	}
}
