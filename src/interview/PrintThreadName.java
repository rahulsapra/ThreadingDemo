/**
 * program to create three threads and print their names in order. thread 1, thread 2, thread 3, thread 1 and so on
 */

package interview;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class ThreadPrint {
	private int nextThread = 1;

	public synchronized void printOne() {
		if (nextThread == 1) {
			System.out.println("Thread 1");
			nextThread = 2;
			notifyAll();
		}
	}

	public synchronized void waitForOne() throws InterruptedException {
		while (nextThread != 1) {
			wait();
		}
	}

	public synchronized void printTwo() {
		if (nextThread == 2) {
			System.out.println("Thread 2");
			nextThread = 3;
			notifyAll();
		}

	}

	public synchronized void waitForTwo() throws InterruptedException {
		while (nextThread != 2) {
			wait();
		}
	}

	public synchronized void printThree() {
		if (nextThread == 3) {
			System.out.println("Thread 3");
			nextThread = 1;
			notifyAll();
		}
	}

	public synchronized void waitForThree() throws InterruptedException {
		while (nextThread != 3) {
			wait();
		}
	}

}

class RunnableOne implements Runnable {

	private ThreadPrint threadPrint;

	public RunnableOne() {
		// TODO Auto-generated constructor stub
	}

	public RunnableOne(ThreadPrint threadPrint) {
		this.threadPrint = threadPrint;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (!Thread.interrupted()) {
			try {
				// TimeUnit.MILLISECONDS.sleep(100);
				threadPrint.printOne();
				threadPrint.waitForOne();
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}

	}
}

class RunnableTwo implements Runnable {

	private ThreadPrint threadPrint;

	public RunnableTwo() {
		// TODO Auto-generated constructor stub
	}

	public RunnableTwo(ThreadPrint threadPrint) {
		this.threadPrint = threadPrint;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (!Thread.interrupted()) {
			try {
				// TimeUnit.MILLISECONDS.sleep(100);
				threadPrint.printTwo();
				threadPrint.waitForTwo();
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}

	}

}

class RunnableThree implements Runnable {

	private ThreadPrint threadPrint;

	public RunnableThree() {
		// TODO Auto-generated constructor stub
	}

	public RunnableThree(ThreadPrint threadPrint) {
		this.threadPrint = threadPrint;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (!Thread.interrupted()) {
			try {
				// TimeUnit.MILLISECONDS.sleep(100);
				threadPrint.printThree();
				threadPrint.waitForThree();
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}

	}

}

public class PrintThreadName {
	public static void main(String[] args) {
		ExecutorService executor = Executors.newCachedThreadPool();
		ThreadPrint threadPrint = new ThreadPrint();
		Thread threadOne = new Thread(new RunnableOne(threadPrint));
		Thread threadTwo = new Thread(new RunnableTwo(threadPrint));
		Thread threadThree = new Thread(new RunnableThree(threadPrint));
		executor.execute(threadOne);
		executor.execute(threadTwo);
		executor.execute(threadThree);
		executor.shutdown();
	}

}
