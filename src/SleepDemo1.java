import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SleepDemo1 {
	public static void main(String[] args) {
		ExecutorService executor = Executors.newCachedThreadPool();
		for (int i = 0; i < 5; i++) {
			executor.execute(new SleepRunnable1());
		}
	}
}

class SleepRunnable1 implements Runnable {

	@Override
	public void run() {
		int sleepTime = (int) (10 * Math.random());
		try {
			Thread.sleep(sleepTime);
			System.out.println("sleepTime: " + sleepTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
