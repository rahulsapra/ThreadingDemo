import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Barrier1Runnable implements Runnable {

	@Override
	public void run() {
		System.out.println("barrier 1 reached");
	}

}

class Barrier2Runnable implements Runnable {

	@Override
	public void run() {
		System.out.println("barrier 2 reached");
	}

}

class CyclicBarrierRunnable implements Runnable {

	private CyclicBarrier barrier1 = null;
	private CyclicBarrier barrier2 = null;

	public CyclicBarrierRunnable() {
		// TODO Auto-generated constructor stub
	}

	public CyclicBarrierRunnable(CyclicBarrier barrier1, CyclicBarrier barrier2) {
		super();
		this.barrier1 = barrier1;
		this.barrier2 = barrier2;
	}

	@Override
	public void run() {
		try {
			System.out.println("waiting on barrier 1: "+Thread.currentThread());
			barrier1.await();
			System.out.println("waiting on barrier2: "+Thread.currentThread());
			barrier2.await();
			System.out.println("thread completed: "+Thread.currentThread());
		} catch (InterruptedException | BrokenBarrierException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}

public class CyclicBarrierDemo {
	public static void main(String[] args) {
		CyclicBarrier barrier1 = new CyclicBarrier(2, new Barrier1Runnable());
		CyclicBarrier barrier2 = new CyclicBarrier(2, new Barrier2Runnable());
		ExecutorService executor = Executors.newCachedThreadPool();
//		for(int i=0; i<2; i++) {
			executor.execute(new CyclicBarrierRunnable(barrier1, barrier2));
			executor.execute(new CyclicBarrierRunnable(barrier1, barrier2));
//		}
		executor.shutdown();
	}
}
