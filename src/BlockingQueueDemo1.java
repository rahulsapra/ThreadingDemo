import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

class Toast {
	public enum Status {
		DRY, BUTTERED, JAMMED
	}

	private int id;

	private Status status = Status.DRY;

	public Toast() {
		// TODO Auto-generated constructor stub
	}

	public Toast(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}

// queue to hold the toast objects
class ToastQueue extends LinkedBlockingDeque<Toast>{}

//the toaster class
class Toaster implements Runnable{

	ToastQueue dryQueue;
	private static int count = 0;
	
	public Toaster() {
		// TODO Auto-generated constructor stub
	}
	
	public Toaster(ToastQueue toastQueue) {
		this.dryQueue = toastQueue;
	}
	
	@Override
	public void run() {
		while(!Thread.interrupted()) {
			try {
				TimeUnit.MILLISECONDS.sleep(100);
				//creating a new toast and adding to it initial queue
				Toast toast = new Toast(count);
				count++;
				dryQueue.put(toast);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
}


class Butterer implements Runnable {

	private ToastQueue dryQueue;
	private ToastQueue butterQueue;
	
	public Butterer() {
		// TODO Auto-generated constructor stub
	}
	
	public Butterer(ToastQueue dryQueue, ToastQueue butterQueue) {
		this.dryQueue = dryQueue;
		this.butterQueue = butterQueue;
	}
	
	@Override
	public void run() {
		while(!Thread.interrupted()) {
			try {
				TimeUnit.MILLISECONDS.sleep(100);
				//getting a toast from the initial queue, buttering it and adding it to the butteredqueue
				Toast toast = dryQueue.take();
				toast.setStatus(toast.getStatus().BUTTERED);
				butterQueue.put(toast);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}

class Jammer implements Runnable {

	private ToastQueue butterQueue;
	private ToastQueue finishedQueue;
	
	public Jammer() {
		// TODO Auto-generated constructor stub
	}
	
	public Jammer(ToastQueue butterQueue, ToastQueue finishedQueue) {
		super();
		this.butterQueue = butterQueue;
		this.finishedQueue = finishedQueue;
	}

	@Override
	public void run() {
		while(!Thread.interrupted()) {
			try {
				TimeUnit.MILLISECONDS.sleep(100);
				//jamming a toast and adding it to the final queue
				Toast toast = butterQueue.take();
				toast.setStatus(toast.getStatus().JAMMED);
				finishedQueue.push(toast);
				System.out.println("toast completed: "+toast.getId());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}


public class BlockingQueueDemo1 {
	public static void main(String[] args) {
		ExecutorService executor = Executors.newCachedThreadPool();
		ToastQueue dryQueue = new ToastQueue();
		ToastQueue butterQueue = new ToastQueue();
		ToastQueue finishedQueue = new ToastQueue();
		executor.execute(new Toaster(dryQueue));
		executor.execute(new Butterer(dryQueue, butterQueue));
		executor.execute(new Jammer(butterQueue, finishedQueue));
		executor.shutdown();
		
		
	}
}
