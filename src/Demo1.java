
public class Demo1 {
	public static void main(String[] args) {
		Thread thread1 = new Thread(new MyRunnable1());
		thread1.start();
		System.out.println("executing in main thread");
	}
}

class MyRunnable1 implements Runnable {

	public MyRunnable1() {
		System.out.println("myrunnable class initiated");
	}

	@Override
	public void run() {
		for (int i = 0; i < 3; i++) {
			System.out.println("message");
			Thread.yield();
		}
		System.out.println("myrunnable is destroyed");
	}

}
