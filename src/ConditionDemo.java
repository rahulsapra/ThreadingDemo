import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


class Series {
	Lock lock = new ReentrantLock();
	Condition condition = lock.newCondition();
	boolean isNextEven = true;
	int nextEven = 0;
	int nextOdd = 1;

	public void waitForEven() throws InterruptedException {
		lock.lock();
		try {
			while (!isNextEven) {
				condition.await(); // waiting till the condition is reached
			}
		} finally {
			lock.unlock();
		}
	}

	public void printEven() {
		lock.lock();
		try {

			if (isNextEven) {
				System.out.println(nextEven);
				nextEven += 2;
				isNextEven = false;
				condition.signalAll(); // similar to notifyall
			}
		} finally {
			lock.unlock();
		}

	}

	public void waitForOdd() throws InterruptedException {
		lock.lock();
		try {
			while (isNextEven) {
				condition.await();
			}
		} finally {
			lock.unlock();
		}
	}

	public void printOdd() {
		lock.lock();
		try {
			if (!isNextEven) {
				System.out.println(nextOdd);
				nextOdd += 2;
				isNextEven = true;
				condition.signalAll();
			}
		} finally {
			lock.unlock();
		}
	}

}

class EvenSeries implements Runnable {

	Series series;

	public EvenSeries() {
	}

	public EvenSeries(Series series) {
		this.series = series;
	}

	@Override
	public void run() {
		while (!Thread.interrupted()) {
			try {
				series.waitForEven();
				series.printEven();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

}

class OddSeries implements Runnable {

	Series series;

	public OddSeries() {
	}

	public OddSeries(Series series) {
		this.series = series;
	}

	@Override
	public void run() {
		while (!Thread.interrupted()) {
			try {
				series.waitForOdd();
				series.printOdd();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

}

public class ConditionDemo {
	
	public static void main(String[] args) throws InterruptedException {
		Series series = new Series();
		ExecutorService executor = Executors.newCachedThreadPool();
		executor.execute(new EvenSeries(series));
		executor.execute(new OddSeries(series));
		TimeUnit.MILLISECONDS.sleep(10);
		executor.shutdownNow();
	}
	

}
