import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class UncaughtExceptionHandlerDemo {
	public static void main(String[] args) {
		// Thread.setDefaultUncaughtExceptionHandler(new
		// MyUncaughtExceptionHandler()); // sets the default uncaught exception handler for all the threads
		ExecutorService executor = Executors.newCachedThreadPool(new MyThreadFactory());
		executor.execute(new MyThread());

	}
}

class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {

	@Override
	public void uncaughtException(Thread t, Throwable e) {
		// TODO Auto-generated method stub
		System.out.println("caught: " + e);
	}

}

class MyThreadFactory implements ThreadFactory {

	@Override
	public Thread newThread(Runnable r) {
		Thread t = new Thread(r);
		t.setUncaughtExceptionHandler(new MyUncaughtExceptionHandler());
		System.out.println("thread created: " + t.getName());
		System.out.println("uncaughtexceptionhandler: " + t.getUncaughtExceptionHandler());
		return t;
	}

}

class MyThread implements Runnable {
	public void run() {
		System.out.println("hello");
		throw new RuntimeException();
	}
}
