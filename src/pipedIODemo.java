import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author rahul PIpes were used before BlockingQueue was introduced.
 */

public class pipedIODemo {
	public static void main(String[] args) throws IOException, InterruptedException {
		Sender sender = new Sender();
		Receiver receiver = new Receiver(sender);
		ExecutorService executor = Executors.newCachedThreadPool();
		executor.execute(sender);
		executor.execute(receiver);
		TimeUnit.MILLISECONDS.sleep(400);
		executor.shutdownNow();
	}
}

class Sender implements Runnable {

	private PipedWriter out = new PipedWriter();

	public PipedWriter getPipedWriter() {
		return out;
	}

	@Override
	public void run() {
		try {
			while (true) {
				for (int i = 0; i < 20; i++) {
					out.write("hello");
					TimeUnit.MILLISECONDS.sleep(50);
				}
			}
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.out.println("write interrupted");
		}
	}
}

class Receiver implements Runnable {

	private PipedReader in;
	private Sender sender;

	public Receiver() {
		// TODO Auto-generated constructor stub
	}

	public Receiver(Sender sender) throws IOException {
		in = new PipedReader(sender.getPipedWriter());
	}

	@Override
	public void run() {
		try {
			while (true) {
				System.out.println(in.read());
				Thread.sleep(50);
			}
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.out.println("read interrupted");
		}

	}

}
