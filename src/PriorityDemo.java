import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PriorityDemo {
	public static void main(String[] args) {
		ExecutorService executor = Executors.newCachedThreadPool();
		ExecutorService executor1 = Executors.newFixedThreadPool(3);
		for (int i = 1; i < 10; i++) {
			executor.execute(new PriorityRunnable(i));
			executor1.execute(new PriorityRunnable(i));
		}
		executor.shutdown();
	}

}

class PriorityRunnable implements Runnable {

	int priority;

	public PriorityRunnable() {
	}

	public PriorityRunnable(int priority) {
		super();
		this.priority = priority;
	}

	@Override
	public void run() {
		Thread.currentThread().setPriority(priority);
		System.out.println(Thread.currentThread().getName() + " " + priority);

	}

}
