
public class RunnableDemo {
	public static void main(String[] args) {
		Thread mythread = new Thread(new MyRunnable());
		mythread.start();
		System.out.println("executing in thread main");
	}

}

class MyRunnable implements Runnable {
	int countdown = 10;

	public void run() {
		while (countdown-- > 0) {
			System.out.print(status() + " ");
		}
		System.out.println();
	}

	public String status() {
		return "" + countdown + "(" + (countdown > 0 ? countdown : "LiftOff") + ")";
	}
}
