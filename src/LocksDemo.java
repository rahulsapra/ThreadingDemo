import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LocksDemo {
	public static void main(String[] args) {
		ExecutorService executor = Executors.newCachedThreadPool();
		for (int i = 0; i < 10; i++) {
			executor.execute(new EvenChecker());
		}
		executor.shutdown();
	}
}

class IntGenerator {
	int evenNumber;
	Lock lock = new ReentrantLock();

	public int next() {
		lock.lock();
		try {
			evenNumber++;
			evenNumber++;
			return evenNumber;
		} finally {
			lock.unlock();
		}
	}
}

class EvenChecker implements Runnable {
	IntGenerator intGenerator;

	public EvenChecker() {
		// TODO Auto-generated constructor stub
		intGenerator = new IntGenerator();
	}

	@Override
	public void run() {
		Thread.yield();
		// TODO Auto-generated method stub
		if (intGenerator.next() % 2 == 0) {
			System.out.println(Thread.currentThread().getName() + " even");
		}
	}
}
