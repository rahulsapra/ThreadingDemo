import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReentrantReadWriteLockDemo {
	public static void main(String[] args) {
		My my = new My();
		ExecutorService executor = Executors.newCachedThreadPool();
		executor.execute(new ReadWriteRunnable("first", my));
		executor.execute(new ReadWriteRunnable("second", my));
		System.out.println(my.lst);
		executor.shutdown();
	}
}

class ReadWriteRunnable implements Runnable {

	private My my;
	private String str;

	public ReadWriteRunnable() {
		// TODO Auto-generated constructor stub
	}

	public ReadWriteRunnable(String str, My my) {
		this.str = str;
		this.my = my;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println(str + i);
			my.add(str + i);
			my.getMax();
			try {
				TimeUnit.MILLISECONDS.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}

class My {
	private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

	List<String> lst = new ArrayList<>();

	public void add(String str) {
		lock.writeLock().lock();
		try {
			lst.add(str);
		} finally {
			lock.writeLock().unlock();
		}
	}

	public void getMax() {
		lock.readLock().lock();
		try {
			Collections.max(lst);
		} finally {
			lock.readLock().unlock();
		}
	}

}
