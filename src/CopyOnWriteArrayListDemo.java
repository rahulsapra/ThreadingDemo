import java.util.concurrent.CopyOnWriteArrayList;

public class CopyOnWriteArrayListDemo {
	public static void main(String[] args) {
		CopyOnWriteArrayList<Integer> numbers = new CopyOnWriteArrayList<>();
		numbers.add(1);
		numbers.add(2);
		numbers.add(3);
		numbers.add(4);
		numbers.add(5);
		System.out.println("the initial list: " + numbers);

		//creating a new thread
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					// sleep a little so that for loop below can print part of
					// the list
					Thread.sleep(250);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
				numbers.add(10);
				System.out.println("numbers: " + numbers);
			}
		}).start();;
		
		System.out.println("printing numbers");
		//printing the array
		for(int i : numbers ) {
			System.out.println(i);
		}
	}
}
