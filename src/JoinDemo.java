import java.util.concurrent.TimeUnit;

public class JoinDemo {
	public static void main(String[] args) {
		Sleeper sleepy = new Sleeper("sleepy", 100), grumpy = new Sleeper("grumpy", 100);
		Joiner dopey = new Joiner("Dopey", sleepy), doc = new Joiner("Doc", grumpy);
		grumpy.interrupt();

	}
}

class Sleeper extends Thread {
	private int duration;

	public Sleeper(String name, int duration) {
		super(name);
		this.duration = duration;
		start();
	}

	public void run() {
		try {
			TimeUnit.MILLISECONDS.sleep(duration);
		} catch (InterruptedException e) {
			System.out.println("interrupted isInterrupted(): " + isInterrupted());
		}
		System.out.println(getName() + " has awakened");
	}
}

class Joiner extends Thread {
	private Sleeper sleeper;

	public Joiner(String name, Sleeper sleeper) {
		super(name);
		this.sleeper = sleeper;
		start();
	}

	public void run() {
		try {
			sleeper.join();
		} catch (InterruptedException e) {
			System.out.println("interrupted");
		}
		System.out.println(getName() + "join completed");
	}
}