import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class DaemonThreadFactoryDemo {
	public static void main(String[] args) {
		ExecutorService executor = Executors.newCachedThreadPool(new DaemonThreadFactory());
		for (int i = 0; i < 5; i++) {
			executor.execute(new ThreadFromFactory());
		}
	}
}

class DaemonThreadFactory implements ThreadFactory {

	@Override
	public Thread newThread(Runnable arg0) {
		Thread thread = new Thread(arg0);
		thread.setDaemon(true);
		return thread;
	}

}

class ThreadFromFactory implements Runnable {
	public void run() {
		System.out.println("hello");
	}
}
