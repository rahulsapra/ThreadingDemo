import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Exercise21 {
	public static void main(String[] args) {
		Temp temp = new Temp();
		ExecutorService executor = Executors.newCachedThreadPool();
		executor.execute(new ExRunnable1(temp));
		executor.execute(new ExRunnable2(temp));
	}
}

class Temp {
	boolean waiting = true;

	public synchronized void keepWaiting() throws InterruptedException {
		while (waiting) {
			wait();
		}
	}

	public synchronized void exitWaiting() {
		waiting = false;
		notify();
	}

}

class ExRunnable1 implements Runnable {
	Temp temp;

	public ExRunnable1(Temp temp) {
		// TODO Auto-generated constructor stub
		this.temp = temp;
	}

	@Override
	public void run() {
		try {
			temp.keepWaiting();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("wait is over");
	}

}

class ExRunnable2 implements Runnable {
	Temp temp;

	public ExRunnable2(Temp temp) {
		// TODO Auto-generated constructor stub
		this.temp = temp;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("notified all");
		temp.exitWaiting();

	}

}