import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableDemo {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService executor = Executors.newFixedThreadPool(3);
		List<Future<String>> results = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			results.add(executor.submit(new MyCallable(i)));
		}
		for (Future<String> str : results) {
			System.out.println(str.get());
		}
		executor.shutdown();
	}
}

class MyCallable implements Callable<String> {

	int id;

	MyCallable() {
	}

	MyCallable(int id) {
		this.id = id;
	}

	@Override
	public String call() throws Exception {
		return "MyCallable: " + id;
	}

}
