import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadLocalDemo {
	public static void main(String[] args) {
		ExecutorService executor = Executors.newCachedThreadPool();
		for (int i = 0; i < 5; i++) {
			executor.execute(new ThreadHolderRunnable());
		}
		executor.shutdown();
	}
}

class ThreadLocalHolder {
	private static ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>() {
		private Random rand = new Random(47);

		protected synchronized Integer initialValue() {
			return rand.nextInt(10000);
		}
	};
	// private Random rand = new Random(47);

	public static void increment() {
		threadLocal.set(threadLocal.get() + 1);
	}

	public static void get() {
		System.out.println(threadLocal.get());
	}
}

class ThreadHolderRunnable implements Runnable {

	@Override
	public void run() {
		// TODO Auto-generated method stub
		int i = 0;
		while (i < 5) {
			ThreadLocalHolder.increment();
			System.out.print(Thread.currentThread().getName() + ": ");
			ThreadLocalHolder.get();
			i++;
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Thread.yield();
		}
	}

}
