package blockingqueues;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class BlockingQueueDemo {
	public static void main(String[] args) throws InterruptedException {
//		BlockingQueue<Meal> orderQueue = new LinkedBlockingQueue<>();
		BlockingQueue<Meal> orderQueue = new ArrayBlockingQueue<>(5);
		ExecutorService exec = Executors.newCachedThreadPool();
		exec.execute(new Waiter(orderQueue));
		exec.execute(new Chef(orderQueue));

		Thread.sleep(10);

		exec.shutdownNow();

	}
}

class Chef implements Runnable {

	private BlockingQueue<Meal> orderQueue;
	private static int count = 0;

	public Chef(BlockingQueue<Meal> orderQueue) {
		super();
		this.orderQueue = orderQueue;
	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				orderQueue.add(new Meal(count++));
				TimeUnit.MILLISECONDS.sleep(1);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

class Waiter implements Runnable {
	private BlockingQueue<Meal> orderQueue;

	public Waiter(BlockingQueue<Meal> orderQueue) {
		super();
		this.orderQueue = orderQueue;
	}

	public void run() {
		try {
			while (!Thread.interrupted()) {
				Meal meal = null;
				meal = orderQueue.take();
				System.out.println("delivered meal: " + meal.getId());

			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

class Meal {
	private int id;

	public Meal(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
