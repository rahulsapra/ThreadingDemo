package blockingqueues;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ArrayBlockingQueue has an uppper limit on the number of elements that can be
 * added to the Queue
 */

public class ArrayBlockingQueueDemo {
	public static void main(String[] args) {
		BlockingQueue queue = new ArrayBlockingQueue<>(4);
		ExecutorService executor = Executors.newCachedThreadPool();
		// for(int i=0; i<5; i++) { will throw an exception as we are trying to
		// insert element into a full list
		for (int i = 0; i < 4; i++) {
			executor.execute(new PrepareFood(queue));
		}
		executor.shutdown();
	}
}
