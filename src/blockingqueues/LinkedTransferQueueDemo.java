package blockingqueues;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedTransferQueue;

public class LinkedTransferQueueDemo {
    public static void main(String[] args) throws InterruptedException {
	BlockingQueue<String> queue = new LinkedTransferQueue<>();
	// methods to insert elements
	queue.add("hello");
	queue.put("world");
	queue.offer("hello1");
	System.out.println(queue);

    }
}
