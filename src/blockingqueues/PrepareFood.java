package blockingqueues;

import java.util.concurrent.BlockingQueue;

public class PrepareFood implements Runnable {

	BlockingQueue<Food> foodQueue;
	private static int counter = 0;

	public PrepareFood(BlockingQueue<Food> foodQueue) {
		this.foodQueue = foodQueue;
	}

	@Override
	public void run() {
		counter++;
		Food food = new Food("food: " + counter);
		System.out.println("preparing food: " + food.getName());
		foodQueue.add(food);
	}

}
