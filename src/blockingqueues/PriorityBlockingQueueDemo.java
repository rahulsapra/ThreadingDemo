package blockingqueues;

import java.util.concurrent.PriorityBlockingQueue;

public class PriorityBlockingQueueDemo {
	public static void main(String[] args) throws InterruptedException {
		PriorityBlockingQueue<String> queue = new PriorityBlockingQueue();
		// the element inserted in the PriorityQueue must implement Comparable
		// interface. In this case String class implements comparable interface
		queue.put("hello");
		queue.put("world");
		queue.put("java");
		queue.put("apple");
		System.out.println(queue.take());
	}
}
