package blockingqueues;

import java.util.concurrent.LinkedBlockingQueue;

public class LinkedBlockingQueueDemo {
	public static void main(String[] args) throws InterruptedException {
		LinkedBlockingQueue<String> unbounded = new LinkedBlockingQueue();
		unbounded.add("hello");
		LinkedBlockingQueue<String> bounded = new LinkedBlockingQueue(10);
		bounded.add("bounded");
		bounded.add("hello");
		System.out.println("unbounded: "+unbounded.take());
		System.out.println("bounded: "+bounded);
		System.out.println("polling bounded: "+bounded.take());
	}
}
