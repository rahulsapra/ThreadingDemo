package blockingqueues;

import java.util.concurrent.BlockingQueue;

public class ConsumeFood implements Runnable {

	BlockingQueue<Food> foodQueue;

	public ConsumeFood() {
		// TODO Auto-generated constructor stub
	}

	public ConsumeFood(BlockingQueue<Food> foodQueue) {
		super();
		this.foodQueue = foodQueue;
	}

	@Override
	public void run() {
		try {
			System.out.println("Consuming food: "+foodQueue.take().getName());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
