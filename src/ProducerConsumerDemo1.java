import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ProducerConsumerDemo1 {
	public static void main(String[] args) {
		new Restaurant1();
	}
}

class Meal1 {
	private int orderNum = 0;
	private boolean isDeliveryReady;

	public Meal1(int orderNum) {
		this.orderNum = orderNum;
	}

	public String toString() {
		return "Meal: " + orderNum;
	}

	public boolean isDeliveryReady() {
		return isDeliveryReady;
	}

	public void setDeliveryReady(boolean isDeliveryReady) {
		this.isDeliveryReady = isDeliveryReady;
	}

}

class Chef1 implements Runnable {

	int count;
	Restaurant1 restaurant;

	public Chef1(Restaurant1 restaurant) {
		this.restaurant = restaurant;
	}

	@Override
	public void run() {
		try {
			// without this condition the program will run only once
			while (!Thread.interrupted()) {
				synchronized (this) {
					while (restaurant.meal != null) {
						wait();
					}
				}
				if (++count == 10) {
					System.out.println("created maxinmum meals for the day. Closing...");
					restaurant.executor.shutdownNow();
				}
				System.out.println("Order up");
				// notifying the chef to produce next meal
				synchronized (restaurant.waitPerson) {
					restaurant.meal = new Meal1(count);
					restaurant.waitPerson.notify();
				}
			}
			// TimeUnit.MILLISECONDS.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}

class WaitPerson1 implements Runnable {

	Restaurant1 restaurant;

	public WaitPerson1(Restaurant1 restaurant) {
		this.restaurant = restaurant;
	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				synchronized (this) {
					while (restaurant.meal == null) {
						wait();
					}
				}
				System.out.println("waitperson got " + restaurant.meal);
				// notifying the chef to produce next meal
				synchronized (restaurant.chef) {
					restaurant.meal = null;
					restaurant.chef.notifyAll();
				}
				synchronized (restaurant.busBoy) {
					restaurant.isCleaningRequired = true;
					restaurant.busBoy.notifyAll();
				}
			}
		} catch (InterruptedException e) {
			System.out.println("waitperson interrupted");
		}

	}
}

class BusBoy implements Runnable {

	Restaurant1 restaurant;

	public BusBoy(Restaurant1 restaurant) {
		this.restaurant = restaurant;
	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				synchronized (this) {
					while (!restaurant.isCleaningRequired) {
						wait();
					}
				}

				System.out.println("cleaned the table");
				restaurant.isCleaningRequired = false;
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("busboy interrupted");
		}
	}

}

class Restaurant1 {
	boolean isCleaningRequired;
	ExecutorService executor = Executors.newCachedThreadPool();
	Meal1 meal;
	Chef1 chef = new Chef1(this);
	WaitPerson1 waitPerson = new WaitPerson1(this);
	BusBoy busBoy = new BusBoy(this);

	public Restaurant1() {
		executor.execute(chef);
		executor.execute(waitPerson);
		executor.execute(busBoy);
	}
}
