import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class EvenOddDemo {
	public static void main(String[] args) throws InterruptedException {
		EvenOddSeries evenOddSeries = new EvenOddSeries();
		ExecutorService executor = Executors.newCachedThreadPool();
		executor.execute(new EvenRunnable(evenOddSeries));
		executor.execute(new OddRunnable(evenOddSeries));
		TimeUnit.SECONDS.sleep(5);
		executor.shutdown();

	}
}

/**
 * 
 * @author rahul the class to store the methods for printing the odd and even
 *         numbers. The shared resource
 */
class EvenOddSeries {

	int nextEven = 0;
	int nextOdd = 1;

	boolean isNextEven = true;

	/**
	 * method to print the even numbers
	 */
	synchronized public void printEven() {
		if (isNextEven) {
			System.out.println(nextEven);
			nextEven += 2;
			isNextEven = false;
			// notifyAll();
			notify();
		}
	}

	/**
	 * method to print the odd numbers
	 */
	synchronized public void printOdd() {
		if (!isNextEven) {
			System.out.println(nextOdd);
			nextOdd += 2;
			isNextEven = true;
			// notifyAll();
			notify();
			// both notify and notifyall will work in this case
		}
	}

	/**
	 * 
	 * @throws InterruptedException
	 * 
	 *             wait for even number to be printed
	 * 
	 */
	public synchronized void waitForEven() throws InterruptedException {
		while (!isNextEven) {
			wait();
		}
	}

	/**
	 * 
	 * @throws InterruptedException
	 * 
	 *             wait for odd number to be printed
	 * 
	 */
	public synchronized void waitForOdd() throws InterruptedException {
		while (isNextEven) {
			wait();
		}
	}
}

/**
 * 
 * @author rahul class to print the even numbers
 */
class EvenRunnable implements Runnable {

	EvenOddSeries evenOddSeries;

	public EvenRunnable(EvenOddSeries evenOddSeries) {
		this.evenOddSeries = evenOddSeries;
	}

	@Override
	public void run() {
		while (!Thread.interrupted()) {
			try {
				Thread.sleep(100);
				evenOddSeries.waitForEven();
				evenOddSeries.printEven();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}

/**
 * 
 * @author rahul class to prepare the odd numbers
 */
class OddRunnable implements Runnable {

	EvenOddSeries evenOddSeries;

	public OddRunnable(EvenOddSeries evenOddSeries) {
		this.evenOddSeries = evenOddSeries;
	}

	@Override
	public void run() {
		while (!Thread.interrupted()) {
			try {
				Thread.sleep(100);
				evenOddSeries.waitForOdd();
				evenOddSeries.printOdd();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
