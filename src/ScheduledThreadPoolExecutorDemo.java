import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledThreadPoolExecutorDemo {
    public static void main(String[] args) {
	ScheduledExecutorService executor = Executors.newScheduledThreadPool(4);
	executor.execute(new ScheduledRunnable());
	executor.scheduleAtFixedRate(new ScheduledRunnable(), 2, 5, TimeUnit.SECONDS);
    }
}

class ScheduledRunnable implements Runnable {

    @Override
    public void run() {
	System.out.println(new Date());
    }

}
