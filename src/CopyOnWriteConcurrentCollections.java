
/**
 * There are two CopyOnWrite concurrent collections in java.
 * CopyOnWriteConcurrentList and CopyOnWriteConcurrentSet
 */

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CopyOnWriteConcurrentCollections {
	public static void main(String[] args) {
		CopyOnWriteDemo cow = new CopyOnWriteDemo();
		ExecutorService executor = Executors.newCachedThreadPool();
		executor.execute(new CopyOnWriteRunnable(cow, "first"));
		executor.execute(new CopyOnWriteRunnable(cow, "second"));
		executor.shutdown();
	}
}

class CopyOnWriteRunnable implements Runnable {

	CopyOnWriteDemo cow;
	String str;

	public CopyOnWriteRunnable() {
		// TODO Auto-generated constructor stub
	}

	public CopyOnWriteRunnable(CopyOnWriteDemo cow, String str) {
		// TODO Auto-generated constructor stub
		this.cow = cow;
		this.str = str;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0; i < 10; i++) {
			cow.add(str + i);
			try {
				TimeUnit.MILLISECONDS.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cow.getNames();
		}
	}

}

class CopyOnWriteDemo {
	private CopyOnWriteArrayList<String> names = new CopyOnWriteArrayList<>();

	public void add(String name) {
		names.add(name);
	}

	public void getNames() {
		System.out.println(names);
	}
}
