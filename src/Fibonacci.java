
public class Fibonacci {
	public static void main(String[] args) {
		Thread thread1 = new Thread(new FibonacciRunnable(5));
		thread1.start();
		System.out.println("executing main thread");
		Thread thread2 = new Thread(new FibonacciRunnable(4));
		thread2.start();
		Thread thread3 = new Thread(new FibonacciRunnable(3));
		thread3.start();
		Thread thread4 = new Thread(new FibonacciRunnable(8));
		thread4.start();
	}
}

class FibonacciRunnable implements Runnable {
	int n;

	public FibonacciRunnable() {
	}

	public FibonacciRunnable(int n) {
		this.n = n;
	}

	public void run() {
		System.out.println("N: " + n + " " + fibonacci(n));
	}

	public String fibonacci(int n) {
		String str = "";
		int first = 0;
		int second = 1;
		str += first + " " + second + " ";
		for (int i = 2; i < n; i++) {
			int next = first + second;
			str += next + " ";
			first = second;
			second = next;
		}
		return str;
	}

}
