import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class TaskPortion implements Runnable {

	private CountDownLatch countdownLatch;

	public TaskPortion() {
		// TODO Auto-generated constructor stub
	}

	public TaskPortion(CountDownLatch countdownLatch) {
		super();
		this.countdownLatch = countdownLatch;
	}

	@Override
	public void run() {
		System.out.println("current task executing: " + Thread.currentThread());
		countdownLatch.countDown();
	}

}

class WaitingTask implements Runnable {

	private CountDownLatch countdownLatch;

	public WaitingTask() {
		// TODO Auto-generated constructor stub
	}

	public WaitingTask(CountDownLatch countdownLatch) {
		super();
		this.countdownLatch = countdownLatch;
	}

	@Override
	public void run() {
		// waiting for countdown latch to reach 0
		try {
			countdownLatch.await();
			System.out.println("executing waiting task: " + Thread.currentThread());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

public class CountdownLatchDemo {
	public static void main(String[] args) {
		CountDownLatch countDownLatch = new CountDownLatch(5);
		ExecutorService executor = Executors.newCachedThreadPool();
		for (int i = 0; i < 5; i++) {
			executor.execute(new TaskPortion(countDownLatch));
			executor.execute(new WaitingTask(countDownLatch));
		}
		executor.shutdown();

	}
}
